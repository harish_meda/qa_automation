package com.PKglobal.SeleniumAssignment.Actions;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;

import com.PKglobal.SeleniumAssignment.Base.TestBase;

import com.relevantcodes.extentreports.LogStatus;

public class FinalCartPage extends TestBase {

	public static final Logger LOG = Logger.getLogger(FinalCartPage.class);

	public FinalCartPage() {
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 10);
		PageFactory.initElements(factory, this);

	}

	@FindBy(xpath = "//div[@class='layer_cart_product col-xs-12 col-md-6']//h2[1]")
	public static WebElement spSuccessMessage;

	@FindBy(xpath = "//a[contains(text(),'Color :')]")
	public static WebElement spProductColor;

	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[4]/span[1]/span[1]")
	public static WebElement spProductPrice;

	@FindBy(css = "body.order.hide-left-column.hide-right-column.lang_en:nth-child(2) div.columns-container div.container div.row:nth-child(3) div.center_column.col-xs-12.col-sm-12 div.table_block.table-responsive:nth-child(5) table.table.table-bordered.stock-management-on tbody:nth-child(3) tr.cart_item.first_item.address_0.odd:nth-child(1) td.cart_quantity.text-center > input.cart_quantity_input.form-control.grey:nth-child(2)")
	public static WebElement spProductQuantity;

	@FindBy(xpath = "//span[contains(text(),'Proceed to checkout')]")
	public static WebElement spProceedToCheckOutButton;

	@FindBy(linkText = "Printed Dress")
	public static WebElement spProductTitle;

	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[6]/span[1]")
	public static WebElement spTotalCost;

	@FindBy(xpath = "//span[@class='continue btn btn-default button exclusive-medium']//span[1]")
	public static WebElement continueButton;

	@FindBy(xpath = "cart_summary")
	public static WebElement cartSummary;

	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[5]/div[1]/a[2]/span[1]")
	public static WebElement spPlusButton;

	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[5]/div[1]/a[1]/span[1]/i[1]")
	public static WebElement spDeleteButton;

	@FindBy(xpath = "//span[@id='total_price']")
	public static WebElement totalCartPrice;

	@FindBy(xpath = "//span[@id='total_price']")
	public static WebElement secondCartPrice;

	@FindBy(xpath = "//span[contains(text(),'Add to cart')]")
	public static WebElement addToCartButton;

	public void productHoveringandandAddtoCart() {

		mouseHover(HomePage.firstProduct);
		wait(1);
		HomePage.firstProductTocart.click();
		wait(1);
		HomePage.closeCross.click();
		LOG.info("Added First Product");
		mouseHover(HomePage.secondProduct);
		wait(1);
		HomePage.secondProductToCart.click();
		wait(1);
		HomePage.closeCross.click();
		LOG.info("Added Second Product");
		wait(1);
		mouseHover(HomePage.thirdProduct);
		wait(1);
		HomePage.thirdProductToCart.click();
		wait(1);
		HomePage.closeCross.click();
		LOG.info("Added Third Product");
		wait(1);
		HomePage.cartButton.click();
		wait(1);
	}

	public void verifyPinkDressDetails() {

		String productTitle = FinalCartPage.spProductTitle.getText();
		Assert.assertEquals(productTitle, "Printed Dress");
		test.log(LogStatus.INFO, "Verfied product title" + productTitle);
		String colourAndSize = FinalCartPage.spProductColor.getAttribute("innerHTML").replaceAll("\\p{Cntrl}", "")
				.trim();
		Assert.assertEquals(colourAndSize, "Color : Pink, Size : S");
		test.log(LogStatus.INFO, "Verfied product size and colour" + colourAndSize);
		String spProductQuantity = FinalCartPage.spProductQuantity.getAttribute("value");
		Assert.assertEquals(spProductQuantity, "1");
		test.log(LogStatus.INFO, "Verfied product quantity" + spProductQuantity);

		String spProductPrice = FinalCartPage.spProductPrice.getText();
		Assert.assertEquals(spProductPrice, "$50.99");
		test.log(LogStatus.INFO, "Verfied product price" + spProductPrice);

		LOG.info("Pink dress seleceted and added to cart");
		test.log(LogStatus.INFO, "Pink dress seleceted and added to cart");

	}

	public void verifyWhiteDressDetails() {

		String productTitle = FinalCartPage.spProductTitle.getText();
		Assert.assertEquals(productTitle, "Printed Dress");
		test.log(LogStatus.INFO, "Verfied product title" + productTitle);
		String colourAndSize = FinalCartPage.spProductColor.getAttribute("innerHTML").replaceAll("\\p{Cntrl}", "")
				.trim();
		Assert.assertEquals(colourAndSize, "Color : Beige, Size : S");
		test.log(LogStatus.INFO, "Verfied product size and colour" + colourAndSize);
		String spProductQuantity = FinalCartPage.spProductQuantity.getAttribute("value");
		Assert.assertEquals(spProductQuantity, "1");

		test.log(LogStatus.INFO, "Verfied product quantity" + spProductQuantity);

		String spProductPrice = FinalCartPage.spProductPrice.getText();
		Assert.assertEquals(spProductPrice, "$50.99");
		test.log(LogStatus.INFO, "Verfied product price" + spProductPrice);

		LOG.info("White dress seleceted and added to cart");
		test.log(LogStatus.INFO, "White dress seleceted and added to cart");

	}

	public void SelectMultipleProducts() {

		HomePage.firstProduct.click();
		test.log(LogStatus.INFO, "clicked on item1");

		wait(1);
		FinalCartPage.addToCartButton.click();
		test.log(LogStatus.INFO, "clicked on addToCartButton");
		wait(2);
		FinalCartPage.continueButton.click();
		test.log(LogStatus.INFO, "clicked on continueButton");
		wait(2);
		HomePage.dressesLink.click();
		test.log(LogStatus.INFO, "clicked on dresses tab");
		wait(2);
		HomePage.secondProduct.click();
		test.log(LogStatus.INFO, "clicked on item2");
		wait(6);
		FinalCartPage.addToCartButton.click();
		test.log(LogStatus.INFO, "clicked on addToCartButton");
		wait(2);

		/*
		 * FinalCartPage.continueButton.click(); test.log(LogStatus.INFO,
		 * "clicked on continueButton"); wait(2); HomePage.dressesLink.click();
		 * test.log(LogStatus.INFO, "clicked on dresses tab"); wait(6);
		 * HomePage.productColourWhite.click();
		 * 
		 * test.log(LogStatus.INFO, "clicked on item3"); wait(6);
		 * FinalCartPage.addToCartButton.click(); test.log(LogStatus.INFO,
		 * "clicked on addToCartButton");
		 */
		wait(2);

		FinalCartPage.spProceedToCheckOutButton.click();
		test.log(LogStatus.INFO, "clicked on proceedTo checkout button");
		wait(2);
		// int firstTableRowCount = tableRowCount();
		mouseHoverAndClick(FinalCartPage.spPlusButton);
		test.log(LogStatus.INFO, "clicked on proceedTo add item button");
		wait(2);
		String totalCartPrice = FinalCartPage.totalCartPrice.getText();

		mouseHoverAndClick(FinalCartPage.spDeleteButton);
		test.log(LogStatus.INFO, "clicked on proceedTo delete item button");
		// int secondTableRowCount = tableRowCount();
		// Assert.assertEquals(firstTableRowCount, secondTableRowCount);
		String SecondTotalCartPrice = FinalCartPage.totalCartPrice.getText();
		Assert.assertEquals(totalCartPrice, SecondTotalCartPrice);

		LOG.info("Multiple products are seleceted and added to cart");
		test.log(LogStatus.INFO, "Multiple products are seleceted and added to cart");

	}

}
