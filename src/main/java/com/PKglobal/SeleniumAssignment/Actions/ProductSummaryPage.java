package com.PKglobal.SeleniumAssignment.Actions;



import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;

import com.PKglobal.SeleniumAssignment.Base.TestBase;

import com.relevantcodes.extentreports.LogStatus;



public class ProductSummaryPage extends TestBase {
	


	
	private static Logger Log = Logger.getLogger(ProductSummaryPage.class.getName());
	

	public ProductSummaryPage() {
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 10);
		PageFactory.initElements(factory, this);

	}

	@FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//img[@class='replace-2x img-responsive']")
	public static WebElement itemDress;

	@FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//div[@class='right-block']//div[@class='content_price']")
	public static WebElement price;

	@FindBy(xpath = "//span[@id='our_price_display']")
	public static WebElement priceOnDescriptionPage;

	@FindBy(xpath = "//p[@id='product_reference']")
	public static WebElement productModel;

	@FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//h5")
	public static WebElement productClass;

	@FindBy(xpath = "//h1[contains(text(),'Printed Dress')]")
	public static WebElement productClassOnDescriptionPage;

	@FindBy(xpath = "//a[@id='color_43']")
	public static WebElement productColourPink;

	@FindBy(xpath = "//a[@id='color_16']")
	public static WebElement productColourWhite;

	@FindBy(xpath = "//img[@id='bigpic']")
	public static WebElement productImage;

	@FindBy(xpath = "//h1[contains(text(),'Printed Dress')]")
	public static WebElement productName;

	@FindBy(xpath = "//span[contains(text(),'New')]")
	public static WebElement productCondition;

	@FindBy(xpath = "//input[@id='quantity_wanted']")
	public static WebElement productQuantity;

	@FindBy(xpath = "//i[@class='icon-plus']")
	public static WebElement plusQuantityButton;

	@FindBy(xpath = "//span[contains(text(),'Add to cart')]")
	public static WebElement addToCartButton;

	@FindBy(xpath = "//i[@class='icon-minus']")
	public static WebElement minusQuantityButton;
	
	@FindBy(xpath = "//span[contains(text(),'Proceed to checkout')]")
	public static WebElement spProceedToCheckOutButton;
	
	@FindBy(xpath = "//div[@class='layer_cart_product col-xs-12 col-md-6']//h2[1]")
	public static WebElement spSuccessMessage;

	public void SelectPinkDress() {

		mouseHoverAndClick(ProductSummaryPage.productColourPink);
		Log.info("Pink dress selected");
		test.log(LogStatus.INFO, "Pink dress selected");

		
		wait(2);
		String ImageUrl = ProductSummaryPage.productImage.getAttribute("src");


		Assert.assertEquals(ImageUrl, "http://automationpractice.com/img/p/1/1/11-large_default.jpg");
		Log.info("Verified colour of the dress");
		test.log(LogStatus.INFO, "Verified colour of the dress");
	
		String productName = ProductSummaryPage.productName.getText();
		Assert.assertEquals(productName, "Printed Dress");
		Log.info("Verified the product name:"+productName);
		test.log(LogStatus.INFO, "Verified the product name:"+productName);
		
		String productCondition = ProductSummaryPage.productCondition.getText();
		Assert.assertEquals(productCondition, "New");
		Log.info("Verified the product condition:"+productCondition);
		test.log(LogStatus.INFO, "Verified the product condition:"+productCondition);
		
		String productQuantity = ProductSummaryPage.productQuantity.getAttribute("value");
		Assert.assertEquals(productQuantity, "1");
		Log.info("Verified the product quantity :"+productQuantity);
		test.log(LogStatus.INFO, "Verified the product quantity:"+productQuantity);
	
		mouseHoverAndClick(ProductSummaryPage.plusQuantityButton);
		String addedProductQuantity = ProductSummaryPage.productQuantity.getAttribute("value");
		Assert.assertEquals(addedProductQuantity, "2");
		Log.info("Verified the  added product quantity :"+addedProductQuantity);
		test.log(LogStatus.INFO, "Verified the product quantity:"+addedProductQuantity);
		
		
		mouseHoverAndClick(ProductSummaryPage.minusQuantityButton);
		String minusProductQuantity = ProductSummaryPage.productQuantity.getAttribute("value");
		Assert.assertEquals(minusProductQuantity, "1");
		Log.info("Verified the  removed product quantity :"+minusProductQuantity);
		test.log(LogStatus.INFO, "Verified the product quantity:"+minusProductQuantity);
		
		click(ProductSummaryPage.addToCartButton);
		test.log(LogStatus.INFO, "Clicked on addCartButton");
		wait(2);
		String SpSuccessMessage = ProductSummaryPage.spSuccessMessage.getText();
		
		System.out.println(SpSuccessMessage);
		Log.info(SpSuccessMessage);
		test.log(LogStatus.INFO, "Verified the message:"+SpSuccessMessage);
		
		
		wait(1);
		ProductSummaryPage.spProceedToCheckOutButton.click();
		wait(1);

	}

	public void SelectWhiteDress() {

		mouseHoverAndClick(ProductSummaryPage.productColourWhite);
		wait(2);
		String ImageUrl = ProductSummaryPage.productImage.getAttribute("src");
		
		Assert.assertEquals(ImageUrl, "http://automationpractice.com/img/p/1/0/10-large_default.jpg");
		Log.info("Verified colour of the dress");
		test.log(LogStatus.INFO, "Verified colour of the dress");
		
		String productName = ProductSummaryPage.productName.getText();
		Assert.assertEquals(productName, "Printed Dress");
		Log.info("Verified the product name:"+productName);
		test.log(LogStatus.INFO, "Verified the product name:"+productName);
		
		String productCondition = ProductSummaryPage.productCondition.getText();
		Assert.assertEquals(productCondition, "New");
		Log.info("Verified the product condition:"+productCondition);
		test.log(LogStatus.INFO, "Verified the product condition:"+productCondition);
		
		String productQuantity = ProductSummaryPage.productQuantity.getAttribute("value");

	
		Assert.assertEquals(productQuantity, "1");
		Log.info("Verified the product quantity :"+productQuantity);
		test.log(LogStatus.INFO, "Verified the product quantity:"+productQuantity);
		
		mouseHoverAndClick(ProductSummaryPage.plusQuantityButton);
		String addedProductQuantity = ProductSummaryPage.productQuantity.getAttribute("value");
		Assert.assertEquals(addedProductQuantity, "2");
		Log.info("Verified the  added product quantity :"+addedProductQuantity);
		test.log(LogStatus.INFO, "Verified the product quantity:"+addedProductQuantity);
		

		mouseHoverAndClick(ProductSummaryPage.minusQuantityButton);
		String minusProductQuantity = ProductSummaryPage.productQuantity.getAttribute("value");
		Assert.assertEquals(minusProductQuantity, "1");
		Log.info("Verified the  removed product quantity :"+minusProductQuantity);
		test.log(LogStatus.INFO, "Verified the product quantity:"+minusProductQuantity);
		
		click(ProductSummaryPage.addToCartButton);
		test.log(LogStatus.INFO, "Clicked on addCartButton");
		wait(2);
		String SpSuccessMessage = ProductSummaryPage.spSuccessMessage.getText();
		System.out.println(SpSuccessMessage);
		Assert.assertEquals("Product successfully added to your shopping cart", "Product successfully added to your shopping cart");
		Log.info(SpSuccessMessage);
		test.log(LogStatus.INFO, "Verified the message:"+SpSuccessMessage);
		

		ProductSummaryPage.spProceedToCheckOutButton.click();
		wait(1);

	}

}
