package com.PKglobal.SeleniumAssignment.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.PKglobal.SeleniumAssignment.Base.TestBase;

import com.relevantcodes.extentreports.LogStatus;




public class HomePage extends TestBase{

	

	private static Logger Log = Logger.getLogger(HomePage.class.getName());
	 

	public HomePage() {

		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 10);
		PageFactory.initElements(factory, this);

	}

	@FindBy(xpath = "//input[@id='search_query_top']")
	public static WebElement searchBox;

	@FindBy(xpath = "//button[@name='submit_search']")
	public static WebElement searchButton;

	@FindBy(xpath ="//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//img[@class='replace-2x img-responsive']")
	public static WebElement productHover;

	@FindBy(xpath = "//span[@class='lighter']")
	public static WebElement resultsMessage;

	@FindBy(xpath = "//p[@class='alert alert-warning']")
	public static WebElement noResultsMessage;

	
	@FindBy(xpath =  "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/ul[1]/li[1]/div[1]/div[1]/div[1]/a[1]/img[1]")
	public static WebElement firstProduct;

	@FindBy(xpath = "//a[@id='color_13']")
	public static WebElement secondProduct;

	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[2]/ul[1]/li[3]/div[1]/div[2]/div[3]/ul[1]/li[1]/a[1]")
	public static WebElement thirdProduct;

	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/header[1]/div[3]/div[1]/div[1]/div[6]/ul[1]/li[2]/a[1]")
	public static WebElement dressesLink;
	
	@FindBy(xpath = "//a[@id='color_16']")
	public static WebElement productColourWhite;
	
	@FindBy(xpath = "//span[@id='our_price_display']")
	public static WebElement priceOnDescriptionPage;
	
	@FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//div[@class='right-block']//div[@class='content_price']")
	public static WebElement price;
	
	@FindBy(xpath = "//h1[contains(text(),'Printed Dress')]")
	public static WebElement productClassOnDescriptionPage;

	@FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//h5")
	public static WebElement productClass;
	
	@FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'Add to cart')]")
	public static WebElement firstProductTocart;
	
	@FindBy(xpath =  "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 first-item-of-tablet-line']//span[contains(text(),'Add to cart')]")
	public static WebElement secondProductToCart;
	
	@FindBy(xpath = "//span[@class='cross']")
	public static WebElement closeCross;
	
	@FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-in-line last-item-of-tablet-line last-item-of-mobile-line']//span[contains(text(),'Add to cart')]")
	public static WebElement thirdProductToCart;
	

	@FindBy(xpath = "//b[contains(text(),'Cart')]")
	public static WebElement cartButton;
	
	

	public void typeInSearchBox(String Item) {

		type(HomePage.searchBox, Item);
		Log.info("Data typed in textbox");
		
	

	}

	public void clickSearchbutton() throws InterruptedException {
	

		HomePage.searchButton.click();
		Log.info("Clicked on Search button");
		test.log(LogStatus.INFO, "Clicked on Search button");
	}

	public void waitForLoad() {
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		Log.info("Wait for loadSS");
		test.log(LogStatus.INFO, "Wait for loadSS");
	}

	public void productHoveringandClick() {

		mouseHoverAndClick(HomePage.productColourWhite);
		Log.info("mousehoveres and clicked on product");
		test.log(LogStatus.INFO, "mousehovered and clicked on product");
		

	}

	public void getResultsMessage() {

		String item = HomePage.searchBox.getAttribute("value").toLowerCase();
		String itemName = HomePage.resultsMessage.getText().toLowerCase();
		String resultItem = itemName.substring(1, itemName.length() - 1);
		Assert.assertEquals(resultItem, item);
		Log.info(item);
		Log.info(resultItem);
		test.log(LogStatus.INFO, resultItem);

	}

	public void getNoResultsMessage() {

		String item = HomePage.searchBox.getAttribute("value").toLowerCase();
		String resultItem = HomePage.noResultsMessage.getText().toLowerCase().replaceAll("\"", "");
		String resultmessage = "no results were found for your search " + item;
		if (item.isEmpty())
			Assert.assertEquals(resultItem, "please enter a search keyword");

		else {

			Assert.assertEquals(resultItem, resultmessage);
		}

		Log.info(item);
		Log.info(resultmessage);
		test.log(LogStatus.INFO, resultmessage);
		
	}

	public void validateProductWithPrice() {

		String productPriceFromHomePage = getProductPriceFromHomePage();

		String priceOnDescriptionPage = HomePage.priceOnDescriptionPage.getText();
		System.out.println("priceOnDescriptionPage  " + priceOnDescriptionPage);
		Assert.assertEquals(productPriceFromHomePage, priceOnDescriptionPage);

	}

	public final String getProductPriceFromHomePage() {

		String productPrice = HomePage.price.getText();
		return productPrice;

	}

	public void verifyDisplayedProducts() {

		int i = 1;

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"center_column\"]/ul")));

		List<WebElement> allElements = driver.findElements(By.xpath("//*[@id=\"center_column\"]/ul"));

		/*
		 * Iterator<WebElement> itr = allElements.iterator(); while (itr.hasNext()) {
		 * Log.info(itr.next().getText()); Log.info(allElements);
		 * 
		 * 
		 * test.log(LogStatus.INFO, allElements.toString().toLowerCase());
		 * 
		 * }
		 */

		for (WebElement element : allElements) {
			System.out.println(allElements);
			Assert.assertTrue(element.getText().toLowerCase().contains("Dress"));
			
			test.log(LogStatus.INFO, allElements.toString().toLowerCase());
					  
			Assert.assertEquals(i, 1);
			i = 0;
			for (WebElement suggestion : allElements) {
				if (element.getText().contentEquals(suggestion.getText())) {
					i++;
				}
			}
		}

	}

	public void validateProductWithClass() {

		String productClass = HomePage.productClass.getText();
		System.out.println("price  " + productClass);
		String productClassOnDescriptionPage = HomePage.productClassOnDescriptionPage.getText();
		System.out.println("priceOnDescriptionPage  " + productClassOnDescriptionPage);
		Assert.assertEquals(productClass, productClassOnDescriptionPage);

	}

	

	
	
}
