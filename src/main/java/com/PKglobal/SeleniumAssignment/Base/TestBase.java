package com.PKglobal.SeleniumAssignment.Base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.PKglobal.SeleniumAssignment.Util.Constants;
import com.PKglobal.SeleniumAssignment.Util.ExtentReportGenerator;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class TestBase {

	public static WebDriver driver;
	

	public static WebDriverWait wait;
	public static ExtentReports rep = ExtentReportGenerator.getInstance();
	public static ExtentTest test;
	public static String browser;
	
	

	public static Logger log = Logger.getLogger(TestBase.class);

	public static void initConfig() {

		if (Constants.browser.equals("firefox")) {

			driver = new FirefoxDriver();
			log.debug("Launching Firefox");
		} else if (Constants.browser.equals("chrome")) {

			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + Constants.chromePath);

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("profile.default_content_setting_values.notifications", 2);
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", prefs);
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-infobars");

			driver = new ChromeDriver(options);
			log.debug("Launching Chrome");
			
		} else if (Constants.browser.equals("ie")) {

			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + Constants.iePath);

			driver = new InternetExplorerDriver();
			log.debug("Launching IE");
		}

		driver.get(Constants.testsiteurl);
		driver.manage().window().maximize();
		

	}

	public static void click(WebElement element) {

		element.click();
		log.debug("Clicking on an Element : " + element);
		test.log(LogStatus.INFO, "Clicking on : " + element);
	}

	public static void type(WebElement element, String value) {

		element.sendKeys(value);

		log.debug("Typing in an Element : " + element + " entered value is :  " + value);

		test.log(LogStatus.INFO, "Typing in Search Box " + " entered value is  " + value);

	}
	
	public int tableRowCount() {
		List<WebElement> rowCount = (List<WebElement>) driver.findElement(By.id("cart_summary"));
		return rowCount.size() ;
		
		
	}
	
	
	protected void mouseHover(WebElement Element) {
		try {
			
			Actions action = new Actions(driver);
			action.moveToElement(Element).build().perform();
			wait(2);
			
		} catch (AssertionError ae) {
			Actions action = new Actions(driver);
			action.moveToElement(Element).build().perform();
			wait(2);
			
		}
		log.info("Hovered Successfully on Sub Menu");
		test.log(LogStatus.INFO, "Hovered Successfully on Sub Menu");
	}
	
	protected void mouseHoverAndClick(WebElement menu) {
		try {
			wait(2);
			Actions action = new Actions(driver);
			action.moveToElement(menu).build().perform();
			wait(2);
			menu.click();
		} catch (AssertionError ae) {
			Actions action = new Actions(driver);
			action.moveToElement(menu).build().perform();
			wait(2);
			menu.click();
		}
		log.info("Clicked Successfully on Sub Menu");
		test.log(LogStatus.INFO, "Hovered Successfully on Sub Menu");
	}

	public static void quitBrowser() {

		driver.quit();

	}
	
	
	public static void wait(int seconds) {
		try {
			Thread.sleep(1000 * seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
