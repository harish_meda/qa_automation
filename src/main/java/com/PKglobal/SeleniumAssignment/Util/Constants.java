package com.PKglobal.SeleniumAssignment.Util;

public class Constants {
	
	
	
	//Configuration
		public static final String browser = "chrome";
		public static final String firefox="firefox";
		public static final String chrome="chrome";
		public static final String ie="ie";
	
		public static final String testsiteurl = "http://www.automationpractice.com";
		public static long implicitwait=10;
		public static final String chromePath="\\src\\test\\resources\\Executors\\chromedriver.exe";
		public static final String iePath="\\src\\test\\resources\\Executors\\IEDriverServer.exe";

} 
