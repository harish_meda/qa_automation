package com.PKglobal.SeleniumAssignment.Util;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


import com.PKglobal.SeleniumAssignment.Base.TestBase;





public class Utils extends TestBase {
	
	
	
	
	private static final Logger LOG = Logger.getLogger(Utils.class);
	

	public static String screenshotPath;
	public static String screenshotName;

	
	
	public static void captureScreenshot() throws IOException {

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		Date d = new Date();
		screenshotName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";

		FileUtils.copyFile(scrFile,
				new File(System.getProperty("user.dir") + "\\target\\surefire-reports\\html\\" + screenshotName));
		LOG.info("Taking screenshot");

	}


}