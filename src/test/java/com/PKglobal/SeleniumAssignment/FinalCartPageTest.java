package com.PKglobal.SeleniumAssignment;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.PKglobal.SeleniumAssignment.Actions.FinalCartPage;
import com.PKglobal.SeleniumAssignment.Actions.HomePage;
import com.PKglobal.SeleniumAssignment.Actions.ProductSummaryPage;
import com.PKglobal.SeleniumAssignment.Base.TestBase;
import com.relevantcodes.extentreports.LogStatus;

public class FinalCartPageTest extends TestBase {

	public static Logger log = Logger.getLogger(FinalCartPageTest.class);

	@BeforeMethod
	public void setUp() throws UnknownHostException {
		log.info("In setup method");
		initConfig();
		
	}

	@Test(groups = { "Verify products in checkout page" })
	public void verifyWhiteProductinCheckoutPage() throws InterruptedException {
		log.info("In verifyWhiteProductinCheckoutPage testcase");

		test.log(LogStatus.INFO, "In verifyWhiteProductinCheckoutPage testcase");
		HomePage home = new HomePage();
		home.typeInSearchBox("dress");
		home.clickSearchbutton();
		wait(2);
		test.log(LogStatus.INFO, "Navigating to product summary page");
		ProductSummaryPage productSummaryPage = new ProductSummaryPage();
		productSummaryPage.SelectWhiteDress();
		wait(2);
		test.log(LogStatus.INFO, "Navigating to final cart page");
		FinalCartPage finalCartPage = new FinalCartPage();
		finalCartPage.verifyWhiteDressDetails();
		test.log(LogStatus.INFO, "Test case executed");
	}

	@Test(groups = { "Verify products in checkout page" })
	public void verifyPinkProductinCheckoutPage() throws InterruptedException {

		log.info("In verifyPinkProductinCheckoutPage testcase");

		test.log(LogStatus.INFO, "In verifyPinkProductinCheckoutPage testcase");

		HomePage home = new HomePage();
		home.typeInSearchBox("dress");
		home.clickSearchbutton();
		wait(2);

		test.log(LogStatus.INFO, "Navigating to product summary page");
		ProductSummaryPage productSummaryPage = new ProductSummaryPage();
		productSummaryPage.SelectPinkDress();
		wait(2);
		test.log(LogStatus.INFO, "Navigating to final cart page");
		FinalCartPage finalCartPage = new FinalCartPage();
		finalCartPage.verifyPinkDressDetails();
		log.info("Testcase completed");
		test.log(LogStatus.INFO, "Test case executed");

	}	

	@Test(groups = { "Verify products in checkout page" })
	public void verifyMultipleProductsInCheckoutPage() throws InterruptedException {

		log.info("In verifyMultipleProductsInCheckoutPage testcase");
	
		test.log(LogStatus.INFO, "In verifyMultipleProductsInCheckoutPage testcase");
		HomePage home = new HomePage();
		home.typeInSearchBox("dress");
		home.clickSearchbutton();
		wait(2);
		test.log(LogStatus.INFO, "Navigating to final cart page");
        FinalCartPage finalCartPage = new FinalCartPage();
		finalCartPage.SelectMultipleProducts();
		test.log(LogStatus.INFO, "Test case executed");
	}

	@AfterMethod
	public void tearDown() {
		quitBrowser();

	}
}
