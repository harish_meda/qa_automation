package com.PKglobal.SeleniumAssignment;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.PKglobal.SeleniumAssignment.Actions.HomePage;
import com.PKglobal.SeleniumAssignment.Actions.ProductSummaryPage;
import com.PKglobal.SeleniumAssignment.Base.TestBase;
import com.relevantcodes.extentreports.LogStatus;



public class ProductSummaryPageTest extends TestBase {
	
	private static Logger Log = Logger.getLogger(ProductSummaryPageTest.class.getName());
	@BeforeMethod
	public void setUp() throws UnknownHostException {
		Log.info("In setup method");
		
	
		initConfig();
	}

	@Test(groups = { "Add Product to cart" })
	public void selectWhiteProduct() throws InterruptedException {

		Log.info("In selectWhiteProduct testcase");
		test.log(LogStatus.INFO, "In selectWhiteProduct testcase");

		
		HomePage home = new HomePage();
		home.typeInSearchBox("dress");
		home.clickSearchbutton();
		wait(2);
		test.log(LogStatus.INFO, "Naviagting to Product summary Page");
		ProductSummaryPage productSummaryPage = new ProductSummaryPage();
		productSummaryPage.SelectWhiteDress();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		

	}

	@Test(groups = { "Add Product to cart" })
	public void selectPinkProduct() throws InterruptedException {

		Log.info("In selectPinkProduct testcase");
		test.log(LogStatus.INFO, "In selectPinkProduct testcase");

		HomePage home = new HomePage();
		home.typeInSearchBox("dress");
		home.clickSearchbutton();
		wait(2);
		test.log(LogStatus.INFO, "Naviagting to Product summary Page");
		ProductSummaryPage productSummaryPage = new ProductSummaryPage();
		productSummaryPage.SelectPinkDress();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		

	}

	@AfterMethod
	public void tearDown() {
		quitBrowser();

	}

}
