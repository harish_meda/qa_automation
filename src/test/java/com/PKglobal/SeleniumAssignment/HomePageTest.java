package com.PKglobal.SeleniumAssignment;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.PKglobal.SeleniumAssignment.Actions.HomePage;
import com.PKglobal.SeleniumAssignment.Base.TestBase;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;




public class HomePageTest extends TestBase {
	
	
	
	private static Logger Log = Logger.getLogger(HomePageTest.class.getName());

@BeforeMethod
	public void setUp() throws UnknownHostException {
	   Log.info("In setup method");
		
	  
		initConfig();
	}

	@Test(groups = { "Product search" })
	public void validSearch() throws InterruptedException {
		System.out.println("122dddfdf");
		
		System.out.println("dddfdf");
		Log.info("In validSearch test");
	
		test.log(LogStatus.INFO, "In validSearch test");

		HomePage home = new HomePage();
		home.typeInSearchBox("dress");
		home.clickSearchbutton();
		wait(10);
		home.getResultsMessage();
		home.verifyDisplayedProducts();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		

	}

	@Test(groups = { "Product search" })
	public void searchWithCombinationOfAlphabetsAndSpecialCharacters() throws InterruptedException {
		Log.info("In searchWithCombinationOfAlphabetsAndSpecialCharacters Test ");
		
		test.log(LogStatus.INFO, "In searchWithCombinationOfAlphabetsAndSpecialCharacters Test");

		HomePage home = new HomePage();
		home.typeInSearchBox("'dress'");
		home.clickSearchbutton();
		wait(10);
		home.getResultsMessage();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		
	}

	@Test(groups = { "Product search" })
	public void searchWithCombinationOfAlphabetsAndNumbers() throws InterruptedException {
		Log.info("In searchWithCombinationOfAlphabetsAndNumbers test");
		test.log(LogStatus.INFO, "In searchWithCombinationOfAlphabetsAndNumbers Test");
		
		HomePage home = new HomePage();
		home.typeInSearchBox("56dress");
		home.clickSearchbutton();
		wait(10);
		home.getNoResultsMessage();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		
	}

	@Test(groups = { "Product search" })
	public void searchWithCapitalLetters() throws InterruptedException {
		Log.info("In searchWithCapitalLetters Test");
		test.log(LogStatus.INFO, "In searchWithCapitalLetters Test");
	
		HomePage home = new HomePage();
		home.typeInSearchBox("DRESS");
		home.clickSearchbutton();
		wait(10);
		home.getResultsMessage();
		home.verifyDisplayedProducts();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		
	}

	@Test
	public void searchWithSpecialChar() throws InterruptedException {
		Log.info("In searchWithSpecialChar Test");
		test.log(LogStatus.INFO, "In searchWithSpecialChar Test");
		
		HomePage home = new HomePage();
		home.typeInSearchBox("%");
		home.clickSearchbutton();
		wait(10);
		home.getNoResultsMessage();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		
		
	}

	@Test(groups = { "Product search" })
	public void searchWithNumbers() throws InterruptedException {
		Log.info(" In searchWithNumbers Test");
		test.log(LogStatus.INFO, "In searchWithNumbers Test");
		
		
		HomePage home = new HomePage();
		home.typeInSearchBox("9");
		home.clickSearchbutton();
		wait(10);
		home.getNoResultsMessage();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
	

	}

	@Test(groups = { "Product search" })
	public void searchWithNoData() throws InterruptedException {
		Log.info(" In searchWithNoData Test");
		test.log(LogStatus.INFO, "In searchWithNoData Test");
	
		HomePage home = new HomePage();
		test.log(LogStatus.INFO, "No data typed in search box");
		home.clickSearchbutton();
		wait(10);
		home.getNoResultsMessage();
		Log.info("Test case executed");
		test.log(LogStatus.INFO, "Test case executed");
		
	}

	@AfterMethod
	public void tearDown() {
		Log.info("In tearDown");
		quitBrowser();
		

	}

}
